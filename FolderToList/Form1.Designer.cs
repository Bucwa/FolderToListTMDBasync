﻿namespace FolderToList
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileListBox = new System.Windows.Forms.ListBox();
            this.openFileButton = new System.Windows.Forms.Button();
            this.selectButton = new System.Windows.Forms.Button();
            this.producersBox = new System.Windows.Forms.TextBox();
            this.directorsBox = new System.Windows.Forms.TextBox();
            this.starsBox1 = new System.Windows.Forms.TextBox();
            this.movieTitleBox = new System.Windows.Forms.TextBox();
            this.ratingLabel = new System.Windows.Forms.Label();
            this.producersLabel = new System.Windows.Forms.Label();
            this.directorsLabel = new System.Windows.Forms.Label();
            this.starsLabel = new System.Windows.Forms.Label();
            this.runTimeLabel = new System.Windows.Forms.Label();
            this.releaseDateLabel = new System.Windows.Forms.Label();
            this.synopsisLabel = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.getInfoButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.genreBox1 = new System.Windows.Forms.TextBox();
            this.sortButton = new System.Windows.Forms.Button();
            this.sortByNameBtn = new System.Windows.Forms.Button();
            this.sortByRatingBtn = new System.Windows.Forms.Button();
            this.sortByDateBtn = new System.Windows.Forms.Button();
            this.numberOfFilesLabel = new System.Windows.Forms.Label();
            this.numberOfDirsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileListBox
            // 
            this.fileListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.fileListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fileListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileListBox.ForeColor = System.Drawing.SystemColors.Info;
            this.fileListBox.FormattingEnabled = true;
            this.fileListBox.HorizontalScrollbar = true;
            this.fileListBox.ItemHeight = 15;
            this.fileListBox.Location = new System.Drawing.Point(0, 2);
            this.fileListBox.Name = "fileListBox";
            this.fileListBox.Size = new System.Drawing.Size(608, 60);
            this.fileListBox.TabIndex = 0;
            this.fileListBox.DoubleClick += new System.EventHandler(this.fileListBox_DoubleClick);
            // 
            // openFileButton
            // 
            this.openFileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(28)))));
            this.openFileButton.FlatAppearance.BorderSize = 0;
            this.openFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openFileButton.Location = new System.Drawing.Point(670, 702);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(122, 36);
            this.openFileButton.TabIndex = 1;
            this.openFileButton.Text = "Browse Files";
            this.openFileButton.UseVisualStyleBackColor = false;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // selectButton
            // 
            this.selectButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(28)))));
            this.selectButton.FlatAppearance.BorderSize = 0;
            this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectButton.Location = new System.Drawing.Point(801, 702);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(122, 36);
            this.selectButton.TabIndex = 4;
            this.selectButton.Text = "Play";
            this.selectButton.UseVisualStyleBackColor = false;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // producersBox
            // 
            this.producersBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.producersBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.producersBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.producersBox.ForeColor = System.Drawing.SystemColors.Info;
            this.producersBox.Location = new System.Drawing.Point(84, 426);
            this.producersBox.Multiline = true;
            this.producersBox.Name = "producersBox";
            this.producersBox.Size = new System.Drawing.Size(340, 42);
            this.producersBox.TabIndex = 10;
            // 
            // directorsBox
            // 
            this.directorsBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.directorsBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.directorsBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directorsBox.ForeColor = System.Drawing.SystemColors.Info;
            this.directorsBox.Location = new System.Drawing.Point(84, 367);
            this.directorsBox.Multiline = true;
            this.directorsBox.Name = "directorsBox";
            this.directorsBox.Size = new System.Drawing.Size(340, 42);
            this.directorsBox.TabIndex = 9;
            // 
            // starsBox1
            // 
            this.starsBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.starsBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.starsBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.starsBox1.ForeColor = System.Drawing.SystemColors.Info;
            this.starsBox1.Location = new System.Drawing.Point(84, 308);
            this.starsBox1.Multiline = true;
            this.starsBox1.Name = "starsBox1";
            this.starsBox1.Size = new System.Drawing.Size(340, 42);
            this.starsBox1.TabIndex = 8;
            // 
            // movieTitleBox
            // 
            this.movieTitleBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.movieTitleBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.movieTitleBox.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.movieTitleBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(178)))), ((int)(((byte)(181)))));
            this.movieTitleBox.Location = new System.Drawing.Point(3, 0);
            this.movieTitleBox.Multiline = true;
            this.movieTitleBox.Name = "movieTitleBox";
            this.movieTitleBox.Size = new System.Drawing.Size(389, 55);
            this.movieTitleBox.TabIndex = 7;
            // 
            // ratingLabel
            // 
            this.ratingLabel.AutoSize = true;
            this.ratingLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ratingLabel.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ratingLabel.ForeColor = System.Drawing.SystemColors.Info;
            this.ratingLabel.Location = new System.Drawing.Point(388, 0);
            this.ratingLabel.Name = "ratingLabel";
            this.ratingLabel.Size = new System.Drawing.Size(0, 28);
            this.ratingLabel.TabIndex = 5;
            // 
            // producersLabel
            // 
            this.producersLabel.AutoSize = true;
            this.producersLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.producersLabel.Location = new System.Drawing.Point(3, 426);
            this.producersLabel.Name = "producersLabel";
            this.producersLabel.Size = new System.Drawing.Size(75, 17);
            this.producersLabel.TabIndex = 4;
            this.producersLabel.Text = "Producers:";
            // 
            // directorsLabel
            // 
            this.directorsLabel.AutoSize = true;
            this.directorsLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directorsLabel.Location = new System.Drawing.Point(3, 367);
            this.directorsLabel.Name = "directorsLabel";
            this.directorsLabel.Size = new System.Drawing.Size(68, 17);
            this.directorsLabel.TabIndex = 4;
            this.directorsLabel.Text = "Directors:";
            // 
            // starsLabel
            // 
            this.starsLabel.AutoSize = true;
            this.starsLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.starsLabel.Location = new System.Drawing.Point(3, 308);
            this.starsLabel.Name = "starsLabel";
            this.starsLabel.Size = new System.Drawing.Size(41, 17);
            this.starsLabel.TabIndex = 4;
            this.starsLabel.Text = "Stars:";
            // 
            // runTimeLabel
            // 
            this.runTimeLabel.AutoSize = true;
            this.runTimeLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runTimeLabel.Location = new System.Drawing.Point(247, 542);
            this.runTimeLabel.Name = "runTimeLabel";
            this.runTimeLabel.Size = new System.Drawing.Size(65, 17);
            this.runTimeLabel.TabIndex = 3;
            this.runTimeLabel.Text = "Run Time";
            // 
            // releaseDateLabel
            // 
            this.releaseDateLabel.AutoSize = true;
            this.releaseDateLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.releaseDateLabel.Location = new System.Drawing.Point(3, 542);
            this.releaseDateLabel.Name = "releaseDateLabel";
            this.releaseDateLabel.Size = new System.Drawing.Size(93, 17);
            this.releaseDateLabel.TabIndex = 3;
            this.releaseDateLabel.Text = "Release Date";
            // 
            // synopsisLabel
            // 
            this.synopsisLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.synopsisLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.synopsisLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.synopsisLabel.ForeColor = System.Drawing.SystemColors.Info;
            this.synopsisLabel.Location = new System.Drawing.Point(3, 72);
            this.synopsisLabel.Multiline = true;
            this.synopsisLabel.Name = "synopsisLabel";
            this.synopsisLabel.ReadOnly = true;
            this.synopsisLabel.Size = new System.Drawing.Size(421, 220);
            this.synopsisLabel.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(381, 559);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // getInfoButton
            // 
            this.getInfoButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(28)))));
            this.getInfoButton.FlatAppearance.BorderSize = 0;
            this.getInfoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.getInfoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getInfoButton.Location = new System.Drawing.Point(932, 702);
            this.getInfoButton.Name = "getInfoButton";
            this.getInfoButton.Size = new System.Drawing.Size(122, 36);
            this.getInfoButton.TabIndex = 6;
            this.getInfoButton.Text = "Get Info";
            this.getInfoButton.UseVisualStyleBackColor = false;
            this.getInfoButton.Click += new System.EventHandler(this.getInfoButton_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 68);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(608, 670);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(666, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(820, 565);
            this.panel1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.genreBox1);
            this.panel2.Controls.Add(this.runTimeLabel);
            this.panel2.Controls.Add(this.movieTitleBox);
            this.panel2.Controls.Add(this.releaseDateLabel);
            this.panel2.Controls.Add(this.ratingLabel);
            this.panel2.Controls.Add(this.producersLabel);
            this.panel2.Controls.Add(this.directorsLabel);
            this.panel2.Controls.Add(this.starsLabel);
            this.panel2.Controls.Add(this.producersBox);
            this.panel2.Controls.Add(this.directorsBox);
            this.panel2.Controls.Add(this.starsBox1);
            this.panel2.Controls.Add(this.synopsisLabel);
            this.panel2.Location = new System.Drawing.Point(390, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(427, 681);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 484);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Genre:";
            // 
            // genreBox1
            // 
            this.genreBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.genreBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.genreBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genreBox1.ForeColor = System.Drawing.SystemColors.Info;
            this.genreBox1.Location = new System.Drawing.Point(84, 484);
            this.genreBox1.Multiline = true;
            this.genreBox1.Name = "genreBox1";
            this.genreBox1.Size = new System.Drawing.Size(340, 42);
            this.genreBox1.TabIndex = 11;
            // 
            // sortButton
            // 
            this.sortButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(100)))), ((int)(((byte)(102)))));
            this.sortButton.FlatAppearance.BorderSize = 0;
            this.sortButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortButton.Location = new System.Drawing.Point(1063, 702);
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size(122, 36);
            this.sortButton.TabIndex = 10;
            this.sortButton.Text = "Ascending";
            this.sortButton.UseVisualStyleBackColor = false;
            this.sortButton.Click += new System.EventHandler(this.sortButton_Click);
            // 
            // sortByNameBtn
            // 
            this.sortByNameBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(28)))));
            this.sortByNameBtn.FlatAppearance.BorderSize = 0;
            this.sortByNameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sortByNameBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortByNameBtn.Location = new System.Drawing.Point(669, 6);
            this.sortByNameBtn.Name = "sortByNameBtn";
            this.sortByNameBtn.Size = new System.Drawing.Size(122, 36);
            this.sortByNameBtn.TabIndex = 11;
            this.sortByNameBtn.Text = "Name";
            this.sortByNameBtn.UseVisualStyleBackColor = false;
            this.sortByNameBtn.Click += new System.EventHandler(this.sortByNameBtn_Click);
            // 
            // sortByRatingBtn
            // 
            this.sortByRatingBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(28)))));
            this.sortByRatingBtn.FlatAppearance.BorderSize = 0;
            this.sortByRatingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sortByRatingBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortByRatingBtn.Location = new System.Drawing.Point(801, 6);
            this.sortByRatingBtn.Name = "sortByRatingBtn";
            this.sortByRatingBtn.Size = new System.Drawing.Size(122, 36);
            this.sortByRatingBtn.TabIndex = 11;
            this.sortByRatingBtn.Text = "Rating";
            this.sortByRatingBtn.UseVisualStyleBackColor = false;
            this.sortByRatingBtn.Click += new System.EventHandler(this.sortByRatingBtn_Click);
            // 
            // sortByDateBtn
            // 
            this.sortByDateBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(28)))));
            this.sortByDateBtn.FlatAppearance.BorderSize = 0;
            this.sortByDateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sortByDateBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sortByDateBtn.Location = new System.Drawing.Point(933, 6);
            this.sortByDateBtn.Name = "sortByDateBtn";
            this.sortByDateBtn.Size = new System.Drawing.Size(122, 36);
            this.sortByDateBtn.TabIndex = 11;
            this.sortByDateBtn.Text = "Date";
            this.sortByDateBtn.UseVisualStyleBackColor = false;
            this.sortByDateBtn.Click += new System.EventHandler(this.sortByDateBtn_Click);
            // 
            // numberOfFilesLabel
            // 
            this.numberOfFilesLabel.AutoSize = true;
            this.numberOfFilesLabel.Location = new System.Drawing.Point(1199, 9);
            this.numberOfFilesLabel.Name = "numberOfFilesLabel";
            this.numberOfFilesLabel.Size = new System.Drawing.Size(0, 13);
            this.numberOfFilesLabel.TabIndex = 12;
            // 
            // numberOfDirsLabel
            // 
            this.numberOfDirsLabel.AutoSize = true;
            this.numberOfDirsLabel.Location = new System.Drawing.Point(1199, 29);
            this.numberOfDirsLabel.Name = "numberOfDirsLabel";
            this.numberOfDirsLabel.Size = new System.Drawing.Size(0, 13);
            this.numberOfDirsLabel.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(1528, 750);
            this.Controls.Add(this.numberOfDirsLabel);
            this.Controls.Add(this.numberOfFilesLabel);
            this.Controls.Add(this.sortByDateBtn);
            this.Controls.Add(this.sortByRatingBtn);
            this.Controls.Add(this.sortByNameBtn);
            this.Controls.Add(this.sortButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.getInfoButton);
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.openFileButton);
            this.Controls.Add(this.fileListBox);
            this.ForeColor = System.Drawing.SystemColors.Info;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Explorer";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox fileListBox;
        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.Button selectButton;
        private System.Windows.Forms.Label ratingLabel;
        private System.Windows.Forms.Label producersLabel;
        private System.Windows.Forms.Label directorsLabel;
        private System.Windows.Forms.Label starsLabel;
        private System.Windows.Forms.Label runTimeLabel;
        private System.Windows.Forms.Label releaseDateLabel;
        private System.Windows.Forms.TextBox synopsisLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button getInfoButton;
        private System.Windows.Forms.TextBox movieTitleBox;
        private System.Windows.Forms.TextBox starsBox1;
        private System.Windows.Forms.TextBox directorsBox;
        private System.Windows.Forms.TextBox producersBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox genreBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.Button sortByNameBtn;
        private System.Windows.Forms.Button sortByRatingBtn;
        private System.Windows.Forms.Button sortByDateBtn;
        private System.Windows.Forms.Label numberOfFilesLabel;
        private System.Windows.Forms.Label numberOfDirsLabel;
    }
}

