﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace FolderToList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        #region TMDB API VARIABLES
        //TMDB API base variables
        static string api_key = "c92d24b28cbac4ed875f6ebe9ecdcc41";
        static string baseURL = "https://api.themoviedb.org/3/";
        static string baseImageURL = "https://image.tmdb.org/t/p/w300";
        static string baseImageURLinfo = "https://image.tmdb.org/t/p/w500";
        static string baseTrailerURL = "https://www.youtube.com/watch?v=";
        #endregion

        #region GLOBAL VARIABLES
        // FILE EXPLORER AND FOLDER SELECT VARAIABLES
        string tempPath;
        string name2;
        string nameAndDate;
        // GET MOVIES FROM FOLDER USING API AND CREATE MOVIE TILES
        string movieTitle;
        string movieDate;
        string movie_id;
        string Title;
        string Date;
        double Rating;
        FlowLayoutPanel FlowLayoutPanel1 = new FlowLayoutPanel();
        TimeSpan myRuntime;
        public RestRequest restRequest;
        List<string> originalPath = new List<string>();
        List<string> oldPath = new List<string>();
        List<PanelControl> controls = new List<PanelControl>(); // ADDS PANEL CONTROLS TO A LIST (HAS DUPLICATES)
        string regexedName;
        IEnumerable<PanelControl> controlWithoutDuplicates; // REMOVES DUPLICATE MOVIE TILES FROM THE FLOW LAYOUT PANEL
        // PANEL CONTROL VARIABLES
        string movieTilesTitle;
        string moviesTilesDate;
        string movieTilesName;
        string movieTilesID;
        string fullpath;
        List<string> fullpaths = new List<string>();
        IEnumerable<string> files;

        bool toggleSort = true; //USED FOR SORT METHODS

        #endregion

        #region FILE EXPLORER AND FOLDER SELECT
        //FOLDER BROWSER DIALOG AND FOLDER SELECT
        private void openFileButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBD = new FolderBrowserDialog();

            if (folderBD.ShowDialog() == DialogResult.OK)
            {
                DirectoryInfo directory = new DirectoryInfo(folderBD.SelectedPath);
                FileInfo[] infos = directory.GetFiles("*", SearchOption.AllDirectories);
                IEnumerable<FileInfo> filtered = infos.Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden)); // FILTERS OUT HIDDEN FILES
                files = filtered.Select(f => f.FullName).ToList().Where(name => name.EndsWith(".mp4") || name.EndsWith(".avi") || name.EndsWith(".mkv") || name.EndsWith(".rmvb"));

                //fileListBox.Items.Clear();
                List<string> dirs = new List<string>(Directory.GetDirectories(folderBD.SelectedPath)); //Array that contains all the directories in the selected directory
                this.tempPath = folderBD.SelectedPath; // GLOBAL STRING

                numberOfFilesLabel.Text = $"Number of files: { files.Count().ToString() }"; //THIS SHOULD COUNT AFTER THE FOREACH LOOP (HOW!!!!)
                numberOfDirsLabel.Text = $"Number of folders: { dirs.Count().ToString() }";

                //RETURNS FILES
                foreach (string file in files)
                {
                    fullpath = Path.GetDirectoryName(file); // RETURNS DIRECTORY WITHOUT FILE NAME
                    var name = Path.GetFileName(file); // RETURNS FILE NAME WITHOUT DIRECTORY
                    //MOVIE NAME WITH PATH
                    originalPath.Add(name); // GLOBAL LIST STRING
                    //MOVIE PATH WITH NO FILE NAME
                    fullpaths.Add(fullpath);

                    //REFINE MOVIE NAME WITH REGEX
                    name2 = regexFunc(name); // GLOBAL STRING

                    //POPULATE LISTBOX;
                    fileListBox.Items.Add($"{name2} {movieDate}");

                    //DISPLAYS A TILE FOR EACH MOVIE IN THE FOLDER
                    nameAndDate = ($"{name2} {movieDate}"); // GLOBAL STRING
                    displayMovieInfo(nameAndDate);
                }

                //RETURNS DIRECTORIES
                foreach (string dir in dirs)
                {
                    fileListBox.Items.Add(Path.GetFileName(dir)); //Adds all the directories to the listBox
                }
            }
        }
        #endregion

        //OPEN SELECTED FILE WITH BUTTON CLICK
        private void selectButton_Click(object sender, EventArgs e) { openMedia(); }

        //OPEN SELECTED FILE WITH A DOUBLE CLICK
        private void fileListBox_DoubleClick(object sender, EventArgs e) { openMedia(); }

        #region OPEN MEDIA WITH BUTTON AND DOUBLE CLICK METHOD
        //OPEN MEDIA WITH BUTTON AND DOUBLE CLICK METHOD
        private void openMedia()
        {
            int index = fileListBox.SelectedIndex;
            string media = originalPath.ElementAt(index);
            string openMedia = Path.Combine(tempPath, media);
            Process.Start(openMedia);
        }
        #endregion

        #region REGEX METHOD FOR FILE NAMES
        //REGEX METHOD FOR FILE NAMES
        private string regexFunc(string name)
        {
            Regex reg = new Regex(@"^(.+?)[.( \t]*(?:(19\d{2}|20(?:0\d|1[0-9])).*|(?:(?=\[|DVDRip|bluray|\d+p|brrip|webrip|CD|BrRip)..*)?[.](divx|rmvb|mkv|avi|mpe?g|mp4)$)");
            Match d = reg.Match(name);
            string movieName = d.Groups[1].Value;
            movieDate = d.Groups[2].Value;
            string movietitle = movieName.Replace(".", " ").Replace("_", " ");
            return movieTitle = movietitle;
        }
        #endregion

        #region GET MOVIE INFO PLUS DETAILS ON BUTTON CLICK
        //GET MOVIE INFO PLUS DETAILS ON BUTTON CLICK API CALL
        private void getInfoButton_Click(object sender, EventArgs e)
        {
            int index = fileListBox.SelectedIndex;
            string movies = originalPath.ElementAt(index);
            movieTitle = regexFunc(movies);
            //displayMovieInfo();
            //getMovieDetails(movie_id);
        }
        #endregion

        #region GET MOVIES FROM FOLDER USING API AND CREATE MOVIE TILES
        //GET MOVIE INFO FROM SELECTED FOLDER AND DISPLAYS THE MOVIE TILES
        private void displayMovieInfo(string nameAndDate)
        {
            RestClient client = new RestClient($"{baseURL}search/movie?api_key={api_key}&query={movieTitle}&year={movieDate}");
            restRequest = new RestRequest(Method.GET);
            restRequest.AddParameter("undefined", "{}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(restRequest);

            // DESERIALIZE JSON RESPONSE
            dynamic m = JObject.Parse(response.Content);

            try
            {
                Title = m.results[0].title;
                Date = m.results[0].release_date;
                //DateFormat date =
                Rating = m.results[0].vote_average;
                movie_id = m.results[0].id;
                PictureBox Poster = new PictureBox();
                Poster.ImageLocation = $"{baseImageURL}{m.results[0].poster_path}";
                createTilesControl(Poster, Title, Date, Rating);
            }
            catch (ArgumentOutOfRangeException)// IGNORES NON-MOVIE FILES AND PREVENTS CRASHING
            {
                
            }
        }
        #endregion

        #region GET MOVIE ID FROM SELECTED POSTER AND DISPLAY
        //GETS THE MOVIE ID FROM A SELECTED PANEL POSTER
        void selectedTile(object sender, EventArgs e)
        {
            PictureBox ps = (PictureBox)sender;
            string v = ps.Tag.ToString();
            getMovieDetails(v);
        }
        #endregion

        #region PLAY MOVIE TILE WITH DOUBLE CLICK
        void playSelected(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            string n = pb.Name;
            List<string> playlist = files.ToList();
            int player = playlist.FindIndex(x => x.Contains(n));
            //string play = files.ElementAt(player);
            //Process.Start();

            var index = files.ToList().IndexOf(n);

            foreach (string item in originalPath)
            {
                regexedName = regexFunc(item);
                oldPath.Add(regexedName);
            }
            int g = oldPath.FindIndex(x => x.Contains(n));
            string media1 = originalPath.ElementAt(g);
            string openMedia1 = Path.Combine(fullpath, media1);
            //Process.Start(openMedia1);
        }
        #endregion

        #region GET MOVIE DATAILS AND DISPLAY METHOD
        //GET MOVIE DETAILS USING MOVIE ID API
        private void getMovieDetails(string movieID)
        {
            movie_id = movieID;

            RestClient client = new RestClient($"{baseURL}movie/{movie_id}?api_key={api_key}&language=en-US&append_to_response=credits");
            restRequest = new RestRequest(Method.GET);
            restRequest.AddParameter("undefined", "{}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(restRequest);

            dynamic d = JObject.Parse(response.Content);

            //POSTER
            pictureBox1.ImageLocation = $"{baseImageURLinfo}{d.poster_path}";
            //MOVIE TITLE
            movieTitleBox.Text = d.title;

            //MOVIE RATING
            ratingLabel.Text = d.vote_average;

            //SYNOPSIS (OVERVIEW)
            synopsisLabel.Text = d.overview;

            //RELEASE DATE
            releaseDateLabel.Text = $"Released: {d.release_date}";

            //MOVIE RUNTIME
            double runtime = d.runtime;
            myRuntime = TimeSpan.FromMinutes(runtime);
            runTimeLabel.Text = $"Runtime: {myRuntime.ToString(@"hh\:mm")}";

            //ACTOR NAMES (CAST CLASS)
            var casting = d.credits.cast;
            List<MovieDetailsHelpers.Cast> cast = casting.ToObject<List<MovieDetailsHelpers.Cast>>();
            var casts = cast.OrderBy(x => x.cast_id).Take(6).Select(x => x.name);//TAKES ONLY THE FIRST 5 ELEMENTS FROM THE LIST AND SELECTS THE NAME FROM EACH
            foreach (string x in casts) { starsBox1.Text = string.Join(", ", casts); }

            //DIRECTOR NAMES (CREW CLASS)
            var crewMembers = d.credits.crew;
            List<MovieDetailsHelpers.Crew> crew = crewMembers.ToObject<List<MovieDetailsHelpers.Crew>>();
            var directors = crew.Where(x => x.job == "Director").Select(x => x.name);
            foreach (string x in directors) { directorsBox.Text = string.Join(", ", directors); }

            //PRODUCER NAMES
            var producers = crew.Where(x => x.job == "Producer").Select(x => x.name);
            foreach (string x in producers) { producersBox.Text = string.Join(", ", producers); }

            //GENRE
            var genreList = d.genres;
            List<MovieDetailsHelpers.Genre> genres = genreList.ToObject<List<MovieDetailsHelpers.Genre>>();
            var genre = genres.Select(x => x.name);
            foreach (string x in genre) { genreBox1.Text = string.Join(", ", genre); }
        }
        #endregion

        #region DYNAMIC CONTROLS FOR MOVIE TILES
        //CREATES GROUPBOX DYNAMICALLY ON RUNTIME (FOR THE MOVIE TILES)
        private void createTilesControl(PictureBox Poster, string Title, string Date, double Rating)
        {
            //TILE POSTER
            Poster.Width = 150;
            Poster.Height = 225;
            Poster.SizeMode = PictureBoxSizeMode.StretchImage;
            Poster.Left = 2;
            Poster.Top = 8;
            Poster.Name = movieTitle;
            Poster.Tag = movie_id;
            Poster.Click += new EventHandler(selectedTile);
            Poster.DoubleClick += new EventHandler(playSelected);

            //TITLE TEXTBOX
            //TextBox titlebox = new TextBox();
            //titlebox.Text = Title;
            //titlebox.Left = 3;
            //titlebox.Top = 236;
            //titlebox.Width = 146;
            //titlebox.Height = 40;
            //titlebox.WordWrap = false;
            //titlebox.Multiline = false;
            //titlebox.BorderStyle = BorderStyle.None;
            //titlebox.ForeColor = Color.FromArgb(255, 255, 255);
            //titlebox.BackColor = Color.FromArgb(45, 45, 48);
            //titlebox.Font = new Font("Century Gothic", 9, FontStyle.Regular);

            Label nameLabel = new Label();
            nameLabel.Text = Title;
            nameLabel.Left = 3;
            nameLabel.Top = 296;
            nameLabel.AutoSize = true;
            nameLabel.ForeColor = Color.FromArgb(255, 255, 255);
            nameLabel.AutoEllipsis = true;
            nameLabel.Font = new Font("Century Gothic", 9, FontStyle.Regular);

            //DATE LABEL
            Label lbDate = new Label();
            lbDate.Text = Date;
            lbDate.Left = 2;
            lbDate.Top = 274;
            lbDate.AutoSize = true;
            lbDate.Font = new Font("Century Gothic", 9, FontStyle.Regular);

            //RATING LABEL
            Label lbRating = new Label();
            lbRating.Text = Rating.ToString();
            lbRating.Left = 127;
            lbRating.Top = 274;
            lbRating.AutoSize = true;
            lbRating.Font = new Font("Century Gothic", 9, FontStyle.Regular);

            //TILE PANEL
            PanelControl movieTiles = new PanelControl();
            movieTiles.Margin = new Padding(20);
            movieTiles.Controls.Add(Poster);
            movieTiles.Controls.Add(nameLabel);
            movieTiles.Controls.Add(lbDate);
            movieTiles.Controls.Add(lbRating);
            movieTiles.Rating = Rating;
            movieTiles.Date = Date;
            moviesTilesDate = movieTiles.Date; //GLOBAL STRING
            movieTiles.Title = movieTitle;
            movieTilesTitle = movieTiles.Title; //GLOBAL STRING
            movieTiles.Name = nameLabel.Text;
            movieTilesName = movieTiles.Name; //GLOBAL STRING
            movieTiles.ID = movie_id;
            movieTilesID = movieTiles.ID; // GLOBAL STRING

            //ADD CONTROLS TO A CONTROL LIST FOR ITERATION
            controls.Add(movieTiles); // GLOBAL LIST CONTROL

            //REMOVES DUPLICATES FROM FLOWLAYOUTPANEL
            controlWithoutDuplicates = controls.GroupBy(x => x.Title).Select(y => y.First()); // GLOBAL IENUMERABLE CONTROL
            foreach (Control panel in controlWithoutDuplicates)
            {
                flowLayoutPanel1.Controls.Add(panel);
            }
        }
        #endregion

        private void sortButton_Click(object sender, EventArgs e)
        {
            sortByName();
            //sortByRating();
            //sortByDate();
        }

        // SORT BY NAME
        private void sortByName()
        {
            sortByRatingBtn.BackColor = Color.FromArgb(26, 26, 28);
            sortByDateBtn.BackColor = Color.FromArgb(26, 26, 28);
            sortByNameBtn.BackColor = Color.FromArgb(16, 100, 102);
            sortByDateBtn.Text = "Date";
            sortByRatingBtn.Text = "Rating";

            if (toggleSort == true)
            {
                var orderByName = controlWithoutDuplicates.OrderBy(x => x.Name);

                foreach (PanelControl item in orderByName)
                {
                    flowLayoutPanel1.Controls.Add(item);
                }
                toggleSort = false;
                sortByNameBtn.Text = "Name A - Z";
            }
            else
            {
                var orderByName = controlWithoutDuplicates.OrderByDescending(x => x.Name);

                foreach (PanelControl item in orderByName)
                {
                    flowLayoutPanel1.Controls.Add(item);
                }
                toggleSort = true;
                sortByNameBtn.Text = "Name Z - A";
            }
        }

        // SORT BY RATING
        private void sortByRating()
        {
            sortByNameBtn.BackColor = Color.FromArgb(26, 26, 28);
            sortByDateBtn.BackColor = Color.FromArgb(26, 26, 28);
            sortByRatingBtn.BackColor = Color.FromArgb(16, 100, 102);
            sortByNameBtn.Text = "Name";
            sortByDateBtn.Text = "Date";

            if (toggleSort == true)
            {
                var orderByRating = controlWithoutDuplicates.OrderBy(x => x.Rating);

                foreach (PanelControl item in orderByRating)
                {
                    flowLayoutPanel1.Controls.Add(item);
                }
                toggleSort = false;
                sortByRatingBtn.Text = "Lowest Rating";
            }
            else
            {
                var orderByRating = controlWithoutDuplicates.OrderByDescending(x => x.Rating);

                foreach (PanelControl item in orderByRating)
                {
                    flowLayoutPanel1.Controls.Add(item);
                }
                toggleSort = true;
                sortByRatingBtn.Text = "Highest Rating";
            }
        }

        // SORT BY DATE
        private void sortByDate()
        {
            sortByNameBtn.BackColor = Color.FromArgb(26, 26, 28);
            sortByRatingBtn.BackColor = Color.FromArgb(26, 26, 28);
            sortByDateBtn.BackColor = Color.FromArgb(16, 100, 102);
            sortByNameBtn.Text = "Name";
            sortByRatingBtn.Text = "Rating";

            if (toggleSort == true)
            {
                var orderByDate = controlWithoutDuplicates.OrderBy(x => x.Date);

                foreach (PanelControl item in orderByDate)
                {
                    flowLayoutPanel1.Controls.Add(item);
                }
                toggleSort = false;
                sortByDateBtn.Text = "Showing Oldest";
            }
            else
            {
                var orderByDate = controlWithoutDuplicates.OrderByDescending(x => x.Date);

                foreach (PanelControl item in orderByDate)
                {
                    flowLayoutPanel1.Controls.Add(item);
                }
                toggleSort = true;
                sortByDateBtn.Text = "Showing Newest";
            }
        }

        // SORT BY GENRE
        private void sortByGenre()
        {

        }

        private void sortByNameBtn_Click(object sender, EventArgs e) { sortByName(); }


        private void sortByRatingBtn_Click(object sender, EventArgs e) { sortByRating(); }


        private void sortByDateBtn_Click(object sender, EventArgs e) { sortByDate(); }

    }


}
