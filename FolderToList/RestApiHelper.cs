﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderToList
{
    public class RestApiHelper<T>
    {
        static string api_key = "c92d24b28cbac4ed875f6ebe9ecdcc41";

        public RestClient restClient;
        public RestRequest restRequest;
        public string baseUrl = "https://reqres.in/";

        public RestClient SetUrl(string resourceUrl)
        {
            var url = Path.Combine(baseUrl, resourceUrl);
            var restClient = new RestClient(url);
            return restClient;
        }

        public RestRequest CreatePostRequest(string jsonString)
        {
            restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return restRequest;
        }

        public IRestResponse GetResponse(RestClient restclient, RestRequest restRequest)
        {
            return restclient.Execute(restRequest);
        }

        public DTO GetContent<DTO>(IRestResponse response)
        {
            var content = response.Content;
            DTO deserializeObject = JsonConvert.DeserializeObject<DTO>(content);
            return deserializeObject;
        }
    }
}
