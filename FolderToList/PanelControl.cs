﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace FolderToList
{
    public class PanelControl : Panel
    {
        public string Title { get; set; } = "Movie name";

        public double? Rating { get; set; } = 1.0;

        public string Date { get; set; } = "";

        public string ID { get; set; }

        public string PathAndName { get; set; }

        public PanelControl()
        {
            Width = 154;
            Height = 294;
            BackColor = Color.FromArgb(45, 45, 48);
            ForeColor = SystemColors.Info;
        }

        Comparison<PanelControl> PanelControlComparer = new Comparison<PanelControl>(ComparePanelControl);



        public static int ComparePanelControl(PanelControl x, PanelControl y)
        {
            return x.Title.CompareTo(y.Title);
        }





        protected override void OnPaint(PaintEventArgs prevent)
        {
            prevent.Graphics.FillRectangle(new SolidBrush(this.BackColor), 0, 0, this.Width, this.Height);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            BackColor = Color.DarkGray;
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            BackColor = Color.FromArgb(45, 45, 48);
        }

    }
}
